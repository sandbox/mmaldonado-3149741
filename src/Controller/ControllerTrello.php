<?php

namespace Drupal\trello\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\trello\TrelloService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ControllerTrello.
 *
 * @category ControllerTrello_Class
 * @package Drupal\trello\Controller
 * @license https://opensource.org/licenses/MIT MIT License
 * @link http://localhost/
 */
class ControllerTrello extends ControllerBase {

  /**
   * @var \Drupal\trello\Controller\TrelloService
   */
  protected $trello_service;

  /**
   * Class constructor.
   */
  public function __construct(TrelloService $trello_service) {
    $this->trello_service = $trello_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('trello.trello_service')
    );
  }

  /**
   * @param $user
   * @param $id
   *
   * @return array
   */
  public function trelloBoardPage($user, $id) {
    $build = [];
    if (!empty($id)) {
      $lists_column = $this->trello_service->getListOptionsbyBoardid($id);
      $boardinfo = $this->trello_service->getBoardInformation($id);
      $listboards = $this->trello_service->getListBoardsUser();

      if (!empty($lists_column)) {
        $build = [
          '#theme' => 'trello_board_theme',
          '#lists' => $lists_column,
          '#board' => $boardinfo,
          '#boards' => $listboards,
        ];
        $build['#attached']['library'][] = 'trello/trello-library';
        $build['#attached']['library'][] = 'trello/trello-board';
        $build['#cache'] = ['max-age' => 0];
      }
    }

    return $build;
  }

  /**
   * @param $user
   *
   * @return array
   */
  public function trelloDashboardsPage($user) {

    $listboards = $this->trello_service->getListBoardsUser();
    $build = [
      '#theme' => 'trello_dashboards_theme',
      '#boards' => $listboards,
    ];
    $build['#attached']['library'][] = 'trello/trello-library';
    $build['#attached']['library'][] = 'trello/trello-dashboard';
    $build['#cache'] = ['max-age' => 0];

    return $build;
  }

}
