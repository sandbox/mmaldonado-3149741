<?php

/**
 * @file
 * Contains \Drupal\trello\TrelloService.
 */

namespace Drupal\trello;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityManagerInterface;

class TrelloService {

  /**
   * @var AccountInterface $current_user
   */
  protected $current_user;

  /**
   * Entity manager.
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entity_manager;

  /**
   * @var string
   */
  protected $base_path;

  /**
   * @var string
   */
  protected $trello_token;

  /**
   * @var string
   */
  protected $trello_key;

  /**
   * When the service is created, set variables.
   */
  public function __construct(AccountInterface $current_user, EntityManagerInterface $entity_manager) {

    $this->current_user = $current_user;
    $this->entity_manager = $entity_manager;
    $this->setValue('base_path', 'https://api.trello.com/1');

    // Set the key and token of the current user, if available.
    $user_storage = $this->entity_manager->getStorage('user');
    $user = $user_storage->load($this->current_user->id());
    $key = $user->get('field_trello_key')->value;
    $token = $user->get('field_trello_token')->value;
    if (!empty($key) && !empty($token)) {
      $this->setValue('key', $key);
      $this->setValue('token', $token);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('entity.manager')
    );
  }

  /**
   * Set the value of a variable.
   */
  public function setValue($arg, $value) {
    switch ($arg) {
      case 'base_path':
        $this->base_path = $value;
        break;
      case 'token':
        $this->trello_token = $value;
        break;
      case 'key':
        $this->trello_key = $value;
        break;
    }
  }

  /**
   * Return the value of a variable.
   */
  public function getValue($arg) {
    switch ($arg) {
      case 'base_path':
        return $this->base_path;
      case 'token':
        return $this->trello_token;
      case 'key':
        return $this->trello_key;
    }
  }

  /**
   * Make a Trello request.
   */
  public function request($path, $query_extra = []) {

    $client = \Drupal::httpClient();
    $query['query'] = [
        'key' => $this->getValue('key'),
        'token' => $this->getValue('token'),
      ] + $query_extra;
    $path = $this->getValue('base_path') . '/' . $path;
    $path = Url::fromUri($path, $query)->toString();
    try {
      $request = $client->get($path);
      $response = json_decode($request->getBody());
    }
    catch (RequestException $e) {
      watchdog_exception('trello', $e->getMessage());
      $response = [];
    }
    return $response;

  }

  /**
   * @param array $extra
   *  Examples $extra = [ 'filter' => 'open', 'members' => 'true', 'member_fields' => 'fullName', 'actions' => 'commentCard', 'attachments' => 'true', 'attachment_fieldss' => 'name,url', 'checklists' => 'all', ];
   *
   * @return array
   */
  public function getListBoardsUser($extra = []) {
    $extra += [
      'filter' => 'open',
      'fields' => 'id,name,url',
    ];
    if (empty($this->getValue("token")) || empty($this->getValue("key"))) {
      return [];
    }
    if ($response = $this->request('members/me/boards', $extra)) {
      return $response;
    }
    return [];
  }

  /**
   * Get list of open lists on the specified board.
   */
  public function getListOptionsbyBoardid($board) {
    if (empty($board) || empty($this->getValue("token")) || empty($this->getValue("key"))) {
      return [];
    }
    $path = 'board/' . $board . '/lists';
    $extra = [
      'cards' => 'open',
      'fields' => 'id,name,url',
    ];
    if ($response = $this->request($path, $extra)) {
      return $response;
    }
    return [];
  }

  /**
   * Get information of board by ID.
   *
   * @param int $board_id
   *   Board Id.
   * @param array $extra
   *   Array of extra parameters.
   *
   * @return array
   *   Array of information Board.
   * @see https://developer.atlassian.com/cloud/trello/rest/#api-boards-id-get
   */
  public function getBoardInformation($board_id, $extra = []) {
    if (empty($board_id) || empty($this->getValue("token")) || empty($this->getValue("key"))) {
      return [];
    }
    $extra += [
      'filter' => 'open',
    ];
    if ($response = $this->request('boards/' . $board_id, $extra)) {
      return $response;
    }
    return [];
  }

}
